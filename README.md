# STEP PROJECT CARDS

## Created by Artem Shchelinskyi, Sergii Tovstuha &  Vitalii Pasechnyk 


### Technologies:


* **HTML, CSS, JS;**
* **GIT;**
* **SCSS;**
* **Gulp;**
* **ES6 modules;**
* **ES6 classes;**
* **AJAX requests (fetch);**
* **Plugins: "browser-sync","del", "fs", "gulp", "gulp-autoprefixer", "gulp-clean-css", "gulp-file-include", "gulp-fonter", "gulp-group-css-media-queries", "gulp-if", "gulp-imagemin", "gulp-newer", "gulp-notify", "gulp-plumber", "gulp-purifycss", "gulp-rename", "gulp-replace", "gulp-sass", "gulp-svg-sprite", "gulp-ttf2woff2", "gulp-util","gulp-version-number", "gulp-webp", "gulp-webp-html-nosvg", "gulp-webpcss", "gulp-zip", "sass", "swiper", "vinyl-ftp", "webp-converter", "webpack","webpack-stream".**


## Tasks performed by the participants:

**Artem Shchelinskyi:**
- Made config files for Gulp;
- Set up a project architecture (files and directories);
- Made a basic HTML and SCSS skeleton of the project;
- Made SCSS for login modal, visit modal and cards.

**Vitalii Pasechnyk:**
- Worked on getting token logic via login modal;
- Made logic creating login modal and visit modal;
- Made logic loading of all cards after authorisation;
- Worked on ajax requests; 
- Worked on the logic of creating, modifying and deleting card;
- Made the whole filter logic;

**Sergii Tovstuha**
- Made filter bar;
- Made the whole filter logic;
- Worked on Drag&Drop logic;
- Testing page components;



### Start local server

```sh
npm run dev
```

### Create production build

```sh
npm run build
```


### Page link

https://shchelinskyi.gitlab.io/step-project-cards

email: asv@gmail.com;
password: asv123;



